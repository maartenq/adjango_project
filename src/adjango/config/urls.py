# config/urls.py

from django.urls import path, include

urlpatterns = [
    path("status/", include("status.urls")),
]
