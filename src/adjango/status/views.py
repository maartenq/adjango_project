# src/store/status/views.py

from django.db import connection
from django.http import JsonResponse

def index(request):
    try:
        with connection.cursor() as cursor:
            cursor.execute("SELECT 1")
        return JsonResponse(
            dict(message="OK"),
            status=200,
        )
    except Exception as err:
        return JsonResponse(
            dict(error=str(err)),
            status=500,
        )
