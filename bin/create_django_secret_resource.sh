#!/bin/sh

SECRET=$(python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())')

kubectl create secret generic adjango-secret \
    --from-literal=DJANGO_SECRET_KEY="${SECRET}"
